#!/home/hz/.rbenv/shims/ruby

def bubbleSort(arr)
  arr[0..(arr.length - 2)].each_with_index do |x, index|
    if x > arr[index + 1] && arr[index + 1]
      arr = swap(arr, index)
      return bubbleSort(arr)
    end
  end
  return arr
end

def swap(arr, index)
  arr[index], arr[index + 1] = arr[index + 1], arr[index]
  return arr
end

def bubbleSortBy(arr)
  finished = false
  while finished == false
    finished = true
    arr[0..((arr.length) - 2)].each_with_index do |x, index|
      if yield(arr[index],arr[index + 1]) > 0
        arr[index], arr[index + 1] = arr[index + 1], arr[index]
        finished = false
      end
    end
  end
  puts "After bubbleSortBy: " + arr.join(',')
end

arr = [2,6,3,4,78,9,53,1]
puts "Before bubbleSort: " + arr.join(",")
arr = bubbleSort(arr)
puts "After bubbleSort: " + arr.join(",")

arr = ['hi','hello','hey','yo','goodday','morning']
puts "Before bubbleSortBy: #{arr}"
bubbleSortBy(arr) do |left,right|
  left.length - right.length
end

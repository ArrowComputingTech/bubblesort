This program is a demonstration of a bubble sorting algorithm, used to sort an array.
The first method, bubbleSort, takes an array and returns it sorted from smallest to largest.
The second method, bubbleSortBy, does the same but sorts by string length, and sorts using a block with yield.
